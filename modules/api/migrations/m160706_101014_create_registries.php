<?php

use yii\db\Migration;

/**
 * Handles the creation for table `registries`.
 */
class m160706_101014_create_registries extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('site_registries_registry', [
            'id' => $this->primaryKey(),
            'company_name' => $this->string(),
            'cert_number' => $this->string(),
            'conformity' => $this->text(),
            'cert_obj' => $this->text(),
            'inn' => $this->string(12),
            'date_start' => $this->date(),
            'date_finish' => $this->date(),
            'address' => $this->string(),
            'expert_name' => $this->string(100),
        ]);
        $this->createIndex('inn_pk', 'site_registries_registry', 'inn');
        $this->createIndex('cert_number_pk', 'site_registries_registry', 'cert_number');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('site_registries_registry');
    }
}
