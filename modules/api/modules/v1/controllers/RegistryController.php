<?php

namespace app\modules\api\modules\v1\controllers;

use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use Yii;
use yii\web\NotFoundHttpException;
use app\modules\api\models\Registries;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class RegistryController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
                'charset' => 'UTF-8',
            ],
        ];
        // ограничивает доступ по ip к перечисленным методам
        /*
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create'],
            'rules' => [
                [
                    'allow' => true,
                    'ips' => ['127.0.0.2'],
                ],
            ],
            'denyCallback' => function($rule, $action) {
                throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
            },
        ];
        */
        return $behaviors;
    }

    public function actions()
    {
        return [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * Возвращает список всех записей из registry
     *
     * @return string
     */
    public function actionIndex()
    {
        $result = new ActiveDataProvider([
            'query' => Registries::find(),
        ]);
        $result->pagination = false;
        return $result;
    }

    /**
     * Возвращает одну запись из registry по id
     *
     * @param $id
     * @return array
     */
    public function actionView($id)
    {
        try {
            Yii::$app->getResponse()->setStatusCode(200);
            return [
                'message' => $this->findRegistry($id),
            ];
        } catch (NotFoundHttpException $exc) {
            Yii::$app->response->setStatusCode(404);
            return [
                'message' => "Запись не найдена",
            ];
        }
    }

    /**
     * Создает новую запись в registry
     *
     * @return array
     */
    public function actionCreate() {
        $registry = new Registries();
        if ($registry->load(Yii::$app->request->post()) && $registry->save()) {
            Yii::$app->getResponse()->setStatusCode(201);
            return [
                'message' => "Запись добавлена",
            ];
        }
        Yii::$app->response->setStatusCode(422);
        return [
            'message' => $registry->errors,
        ];
    }
    
    /**
     * Устанавливаем запись по номеру. При отсутствии, запись создастся.
     * @access public
     * @return array
     */
    public function actionSet() {
        $post = Yii::$app->request->post();

        if( !isset( $post[ 'Registries' ][ 'cert_number' ] ) ) {
            return [
                'message' => "Укажите ['Registries']['cert_number']",
            ];
        }

        $cert_number = $post[ 'Registries' ][ 'cert_number' ];
        $registry = Registries::findByCertNumber( $cert_number );
        $newRecord = false;

        if( !$registry ) {
            $registry = new Registries();
            $newRecord = true;
        }
        
        if( $registry->load( $post ) && $registry->save() ) {
            Yii::$app->getResponse()->setStatusCode( 201 );
            return [
                'message'    => "Запись установлена",
                'new_record' => $newRecord
            ];
        }
        
        Yii::$app->response->setStatusCode( 422 );
        return [
            'message'    => $registry->errors,
            'new_record' => $newRecord
        ];
    }

    /**
     * Поиск записи в registry по id
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    protected function findRegistry($id)
    {
        if (($model = Registries::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}