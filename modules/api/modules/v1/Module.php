<?php

namespace app\modules\api\modules\v1;

use Yii;
/**
 * v1 module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\api\modules\v1\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        parent::init();
        Yii::$app->user->enableSession = false;
        // Yii::$app->user->identityClass = \app\models\Users::class;
    }
}
