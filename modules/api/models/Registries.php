<?php

namespace app\modules\api\models;

use Yii;

/**
 * This is the model class for table "{{%registries}}".
 *
 * @property integer $id
 * @property string $company_name
 * @property string $cert_number
 * @property string $conformity
 * @property string $cert_obj
 * @property string $inn
 * @property string $date_start
 * @property string $date_finish
 * @property string $address
 * @property string $expert_name
 */
class Registries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_registries_registry_n';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['conformity', 'cert_obj'], 'string'],
            [['date_start', 'date_finish'], 'safe'],
            [['company_name', 'cert_number', 'address'], 'string', 'max' => 255],
            [['inn'], 'string', 'max' => 12],
            [['expert_name'], 'string', 'max' => 100],
            [
                'cert_number',
                'match',
                'pattern' => '/^(ЕАС)\.(04ИБН0)\.([А-Я]{2})\.(\d{4})$/u',
                'message' => 'Не верный формат номера сертификата.'
            ],
            [['cert_number'], 'unique'],
            [['inn', 'company_name', 'cert_obj', 'cert_number', 'date_start', 'date_finish', 'address', 'conformity'
            ], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_name' => Yii::t('app', 'Company Name'),
            'cert_number' => Yii::t('app', 'Cert Number'),
            'conformity' => Yii::t('app', 'Conformity'),
            'cert_obj' => Yii::t('app', 'Cert Obj'),
            'inn' => Yii::t('app', 'Inn'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_finish' => Yii::t('app', 'Date Finish'),
            'address' => Yii::t('app', 'Address'),
            'expert_name' => Yii::t('app', 'Expert Name'),
        ];
    }
    
    /**
     * Поиск по номеру сертификата
     * @access public
     * @static
     * @param string $cert_number
     * @return Registries|null
     */
    public static function findByCertNumber( $cert_number ) {
        return self::find()->andWhere( [ '=', 'cert_number', $cert_number ] )->one();
    }

}
