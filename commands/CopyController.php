<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\BlockFile;
use app\models\LastUpdatedFile;
use app\models\SiteCsId2office;
use app\models\SiteOffice;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CopyController extends Controller
{
    public function actionIndex() 
    {

        if(BlockFile::isBlocked()){
            return;
        } 
        
        BlockFile::create('blocked', time());
        
        $data = [];

        $modelClass = 'app\models\SiteCountriesStatistics';
        
        $id_cs = implode(", " , SiteCsId2office::getAllIdCs());
        
        //составляем вхере
        $where = $id_cs !== '' ? "`id` NOT IN($id_cs)" : 1;
        
            foreach (
                $modelClass::find()
                ->select(['log_distribute', 'id'])
                ->where($where)    
                ->asArray()
                ->batch(1000) as $requests
            ){
                //проходим по каждому запросу
                foreach ($requests as $request){
 
                    //тащим офисы из запроса - unserialize сильно тормозит процесс
                    $offices = SiteOffice::GetLogOffices(unserialize($request['log_distribute']));
      
                    //идем по офисам
                    foreach ($offices as $office){

                            //тащим из объекта все свойства
                            $array = get_object_vars($office);
                            
                            $data[] = [$request['id'], $array['id']];

                    }
 
                }
 
            }

            if(!empty($data)){
                Yii::$app->db
                ->createCommand()
                ->batchInsert(
                    'site_cs_id2office', 
                    [
                        'cs_id', 
                        'office_id'
                    ], 
                    $data
                )
                ->execute();
            }
            
            BlockFile::delete('blocked');
            LastUpdatedFile::create('updated', time());
    }
}
