<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_question".
 *
 * @property integer $id
 * @property integer $site_id
 * @property integer $city_id
 * @property string $city
 * @property string $region
 * @property integer $id_country
 * @property integer $user_id
 * @property string $question
 * @property string $question_email
 * @property string $question_phone
 * @property string $question_name
 * @property integer $question_time
 * @property string $answer
 * @property string $answer_email
 * @property string $answer_phone
 * @property string $answer_name
 * @property integer $answer_time
 * @property integer $active
 * @property integer $deleted
 * @property string $lang
 * @property string $seo_header
 * @property integer $is_shared
 */
class SiteQuestion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_question';
    }
    
    public function fields()
    {
        $fields = parent::fields();
        
        $fields['site_name'] = function () {
            return $this->sitename->name;
        };
        
        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'id_country', 'question', 'question_email', 'question_phone', 'question_name', 'question_time'], 'required'],
            [['site_id', 'city_id', 'id_country', 'user_id', 'question_time', 'answer_time', 'active', 'deleted', 'is_shared'], 'integer'],
            [['region', 'question', 'answer', 'seo_header'], 'string'],
            [['city', 'question_email', 'question_phone', 'question_name', 'answer_email', 'answer_phone', 'answer_name'], 'string', 'max' => 255],
            [['lang'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_id' => 'Site ID',
            'city_id' => 'City ID',
            'city' => 'City',
            'region' => 'Region',
            'id_country' => 'Id Country',
            'user_id' => 'User ID',
            'question' => 'Question',
            'question_email' => 'Question Email',
            'question_phone' => 'Question Phone',
            'question_name' => 'Question Name',
            'question_time' => 'Question Time',
            'answer' => 'Answer',
            'answer_email' => 'Answer Email',
            'answer_phone' => 'Answer Phone',
            'answer_name' => 'Answer Name',
            'answer_time' => 'Answer Time',
            'active' => 'Active',
            'deleted' => 'Deleted',
            'lang' => 'Lang',
            'seo_header' => 'Seo Header',
            'is_shared' => 'Is Shared',
        ];
    }
    
    public function getSitename(){
        
        return $this->hasOne(SiteSite::className(), ['id' => 'site_id']);
        
    }
}
