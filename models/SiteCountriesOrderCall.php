<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_countries_order_call".
 *
 * @property integer $id
 * @property integer $id_order_call
 * @property integer $id_country
 * @property integer $id_city
 * @property integer $id_region
 * @property string $algoritm
 */
class SiteCountriesOrderCall extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_countries_order_call';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_order_call', 'id_country', 'id_city', 'id_region'], 'integer'],
            [['id_region'], 'required'],
            [['algoritm'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_order_call' => 'Id Order Call',
            'id_country' => 'Id Country',
            'id_city' => 'Id City',
            'id_region' => 'Id Region',
            'algoritm' => 'Algoritm',
        ];
    }
}
