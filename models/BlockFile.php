<?php

namespace app\models;

use Yii;
/**
 * Класс для работы с фалом блокировки
 *
 * @author Max
 */
class BlockFile extends File{
    
    /**
     * Проверка блокировки
     * Если файл существует и он существует менее 10 минут - блокировка активна
     * в противном случае блокировка не активна, возможно что-то пошло не так
     * @return boolean
     */
    public static function isBlocked(){
        
        //если файл блокировки сущетсвует
        if(file_exists(Yii::getAlias('@runtime').'/blocked')){
            
            //тащим время блокировки
            $timeBlocked = file_get_contents(Yii::getAlias('@runtime').'/blocked');
            
            $timePassed = time() - $timeBlocked;
            
            //если прошло менее 10 минут - блокировка активна
            if($timePassed < 600){
                return true;
            }
            
        }
        
        //блокировки нет
        return false;
        
    }
    
}
