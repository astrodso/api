<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_cs_id2office".
 *
 * @property integer $cs_id
 * @property integer $office_id
 */
class SiteCsId2office extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_cs_id2office';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cs_id', 'office_id'], 'required'],
            [['cs_id', 'office_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cs_id' => 'Cs ID',
            'office_id' => 'Office ID',
        ];
    }
    
    public static function getAllIdCs(){
        
        $idS = [];
        
        $query = self::find();
        
        $result = $query
                ->select(['cs_id'])
                ->asArray()
                ->groupBy('cs_id')
                ->all(); 
        
        foreach ($result as $array){
            $idS[] = $array['cs_id'];
        }
        
        return $idS;
        
    }
    
}
