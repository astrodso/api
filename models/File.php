<?php

namespace app\models;

use Yii;
/**
 * Класс для работы с фалами
 *
 * @author Max
 */
class File extends \yii\base\Object{

    /**
     * создает файл и записывает в него строку
     */
    public static function create( $name, $string = ''){
        
        file_put_contents(self::getDir()."/$name", $string);
        
    }
    
    /**
     * удаляет файл
     */
    public static function delete( $name ){
        
        unlink(self::getDir()."/$name");
        
    }
    
    public static function getDir(){
        
        return Yii::getAlias('@runtime');
        
    }
    
}
