<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_order_certificate_new".
 *
 * @property integer $id
 * @property string $required_document
 * @property string $name_product
 * @property string $code_okp
 * @property string $code_tnvd
 * @property string $certification_scheme
 * @property string $origin
 * @property string $name
 * @property string $telephone
 * @property string $email
 * @property string $city
 * @property string $region
 * @property string $lang
 * @property integer $id_country
 * @property integer $id_site
 * @property string $date_create
 * @property integer $is_shared
 * @property integer $is_deleted
 */
class SiteOrderCertificateNew extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_order_certificate_new';
    }
    
    public function fields()
    {
        $fields = parent::fields();
        
        $fields['site_name'] = function () {
            return $this->sitename->name;
        };
        
        $fields['country_name'] = function () {
            return $this->countryname->name;
        };
        
        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['required_document', 'name_product', 'code_okp', 'code_tnvd', 'certification_scheme', 'origin', 'name', 'telephone', 'email', 'city', 'region', 'lang'], 'string'],
            [['id_country', 'id_site', 'is_shared', 'is_deleted'], 'integer'],
            [['date_create'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'required_document' => 'Required Document',
            'name_product' => 'Name Product',
            'code_okp' => 'Code Okp',
            'code_tnvd' => 'Code Tnvd',
            'certification_scheme' => 'Certification Scheme',
            'origin' => 'Origin',
            'name' => 'Name',
            'telephone' => 'Telephone',
            'email' => 'Email',
            'city' => 'City',
            'region' => 'Region',
            'lang' => 'Lang',
            'id_country' => 'Id Country',
            'id_site' => 'Id Site',
            'date_create' => 'Date Create',
            'is_shared' => 'Is Shared',
            'is_deleted' => 'Is Deleted',
        ];
    }
    
    public function getSitename(){
        
        return $this->hasOne(SiteSite::className(), ['id' => 'id_site']);
        
    }
    
    public function getCountryname(){
        
        return $this->hasOne(SiteCountriesList::className(), ['id' => 'id_country']);
        
    }
    
}
