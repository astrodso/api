<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_countries_order_certificate".
 *
 * @property integer $id
 * @property integer $id_order_certificate
 * @property integer $id_country
 * @property integer $id_city
 * @property integer $id_region
 * @property string $algoritm
 */
class SiteCountriesOrderCertificate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_countries_order_certificate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_order_certificate', 'id_country', 'id_city', 'id_region'], 'integer'],
            [['id_region'], 'required'],
            [['algoritm'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_order_certificate' => 'Id Order Certificate',
            'id_country' => 'Id Country',
            'id_city' => 'Id City',
            'id_region' => 'Id Region',
            'algoritm' => 'Algoritm',
        ];
    }
}
