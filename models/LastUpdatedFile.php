<?php

namespace app\models;

use Yii;
/**
 * Класс для работы с фалом, 
 * содержащим время последнего обновления таблицы site_cs_id2office
 *
 * @author Max
 */
class LastUpdatedFile extends File{

    public static function getDate(){
        
        return date('Y-m-d H:i:s', file_get_contents(self::getDir().'/updated'));

    }
    
}
