<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_site".
 *
 * @property integer $id
 * @property string $name
 * @property string $api_url
 * @property string $api_ipv4
 * @property string $api_ipv4_custom
 * @property string $api_key
 * @property integer $active
 * @property integer $chat
 * @property string $chat_options
 * @property integer $type
 * @property integer $user_id
 * @property integer $time
 */
class SiteSite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_site';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'api_key', 'chat_options'], 'required'],
            [['active', 'chat', 'type', 'user_id', 'time'], 'integer'],
            [['name', 'api_url', 'chat_options'], 'string', 'max' => 255],
            [['api_ipv4', 'api_ipv4_custom'], 'string', 'max' => 15],
            [['api_key'], 'string', 'max' => 32],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'api_url' => 'Api Url',
            'api_ipv4' => 'Api Ipv4',
            'api_ipv4_custom' => 'Api Ipv4 Custom',
            'api_key' => 'Api Key',
            'active' => 'Active',
            'chat' => 'Chat',
            'chat_options' => 'Chat Options',
            'type' => 'Type',
            'user_id' => 'User ID',
            'time' => 'Time',
        ];
    }
}
