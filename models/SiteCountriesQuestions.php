<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_countries_questions".
 *
 * @property integer $id
 * @property integer $id_question
 * @property integer $id_country
 * @property integer $id_city
 * @property integer $type
 * @property integer $id_region
 * @property string $algoritm
 */
class SiteCountriesQuestions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_countries_questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_question', 'id_country', 'id_city', 'type', 'id_region'], 'integer'],
            [['id_region'], 'required'],
            [['algoritm'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_question' => 'Id Question',
            'id_country' => 'Id Country',
            'id_city' => 'Id City',
            'type' => 'Type',
            'id_region' => 'Id Region',
            'algoritm' => 'Algoritm',
        ];
    }

}
