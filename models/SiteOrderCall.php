<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_order_call".
 *
 * @property integer $id
 * @property integer $id_site
 * @property integer $id_country
 * @property string $name
 * @property string $telephone
 * @property string $city
 * @property string $region
 * @property string $lang
 * @property string $time_start
 * @property string $time_end
 * @property string $date_create
 * @property string $office_email
 */
class SiteOrderCall extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_order_call';
    }
    
    public function fields()
    {
        $fields = parent::fields();
        
        $fields['site_name'] = function () {
            return $this->sitename->name;
        };
        
        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_site', 'id_country'], 'integer'],
            [['name', 'telephone', 'city', 'region', 'lang', 'time_start', 'time_end', 'office_email'], 'string'],
            [['date_create'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_site' => 'Id Site',
            'id_country' => 'Id Country',
            'name' => 'Name',
            'telephone' => 'Telephone',
            'city' => 'City',
            'region' => 'Region',
            'lang' => 'Lang',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'date_create' => 'Date Create',
            'office_email' => 'Office Email',
        ];
    }
    
    public function getSitename(){
        
        return $this->hasOne(SiteSite::className(), ['id' => 'id_site']);
        
    }
}
