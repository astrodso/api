<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_countries_statistics".
 *
 * @property integer $id
 * @property integer $id_cq
 * @property string $date_create
 * @property string $log_distribute
 * @property string $type
 * @property integer $is_test
 */
class SiteCountriesStatistics extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'site_countries_statistics';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_cq', 'is_test'], 'integer'],
            [['date_create'], 'safe'],
            [['log_distribute', 'type'], 'string'],
            [['type', 'is_test'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_cq' => 'Id Cq',
            'date_create' => 'Date Create',
            'log_distribute' => 'Log Distribute',
            'type' => 'Type',
            'is_test' => 'Is Test',
        ];
    }
    
    public function getQuestion(){
        
        return $this->hasOne(SiteCountriesQuestions::className(), ['id' => 'id_cq']);
        
    }
    
    public function getO_call(){
        
        return $this->hasOne(SiteCountriesOrderCall::className(), ['id' => 'id_cq']);
        
    }
    
    public function getO_cert(){
        
        return $this->hasOne(SiteCountriesOrderCertificate::className(), ['id' => 'id_cq']);
        
    }

}
