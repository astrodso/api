<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_office".
 *
 * @property integer $id
 * @property string $name
 * @property integer $city_id
 */
class SiteOffice extends \yii\db\ActiveRecord
{
    
    public static $virtual = 'db8857b420665169e36ad60c8dfc23a5';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_office';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['city_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Офис',
            'city_id' => 'City ID',
        ];
    }
    
    public static function getOfficeIdByToken( $token ){
        
        if($token == self::$virtual)
            return false;
        
        //получаем id офиса по токену
        $query = self::find();
        $obj = $query->select(['id'])
                ->where(
                    ['md5(CONCAT(id, name))' => $token]
                )
                ->one();
        
        return $obj->id;
        
    }
    
    /**
     * возвращает данные по офисам из лога
     */
    public static function GetLogOffices($Log) {
        foreach ($Log as $_log) {
            if (isset($_log['data']) && isset($_log['data']['offices']) && is_array($_log['data']['offices'])) {
                return $_log['data']['offices'];
            }
        }

        return array();
    }
    
}
