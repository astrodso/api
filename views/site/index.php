<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SiteUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Токены';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-user-index">
    <p>
        Время последнего обновления связей запросов с офисами: <?= $lastUpdateOffices?>
    </p>
    <p>
        <div><b>Виртуальный офис</b></div>
        <?php $virtualToken = \app\models\SiteOffice::$virtual;?>
        <?= Html::a($virtualToken, 'get?access-token='.$virtualToken, ['target' => '_blank']); ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'format' => 'text',
                'label' => 'Токен',
                'content'=>function($data){
                    return Html::a(
                        md5($data->id.$data->name), 
                        '/get?access-token='.md5($data->id.$data->name),
                        ['target' => '_blank']
                    );
                }
            ],
            'name',
        ],
    ]); ?>
</div>
