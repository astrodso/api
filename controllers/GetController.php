<?php

namespace app\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\data\ActiveDataProvider;
use app\models\SiteOffice;
use app\models\SiteCsId2office;

class GetController extends ActiveController {

    public $modelClass = 'app\models\SiteCountriesStatistics';
    
    //тип запроса к имени класса
    private $typeToQueryModelName = [
        'question' => 'app\models\SiteQuestion',
        'o_call'   => 'app\models\SiteOrderCall',
        'o_cert'   => 'app\models\SiteOrderCertificateNew'
    ];
    
    //тип запроса к имени поля
    private $typeToIdFieldName = [
        'question' => 'id_question',
        'o_call'   => 'id_order_call',
        'o_cert'   => 'id_order_certificate'
    ];

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actions() {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create']);
        
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }
    
    public function prepareDataProvider(){
        
        $modelClass = $this->modelClass;
        $params = \Yii::$app->request->queryParams;
        
        //получаем id офиса по токену
        $officeId = SiteOffice::getOfficeIdByToken( $params['access-token'] );

        //если пришел id запроса
        if(!empty($params['id'])){
            
            //тащим id из таблицы SiteCountriesStatistics
            $query = $modelClass::find();
            $obj = $query->select(['id_cq', 'type'])
                ->where(
                    ['id' => $params['id']]
                )
                ->one();
            
            //какие поля и классы брать, в зависимости от типа запроса из SiteCountriesQuestions
            $fieldName = $this->typeToIdFieldName[$obj->type];
            $queryClassName = $this->typeToQueryModelName[$obj->type];

            //получаем id запроса
            //объект->связанная таблица по имени типа запроса->поле в связанной таблице
            $queryId = $obj->{$obj->type}->$fieldName;
            
            //тащим запрос
            $query = $queryClassName::find()
                ->with('sitename')
                ->where(
                    ['id' => $queryId]
                );
        
        } elseif($officeId) {
            
            $time = time();
            
            //массив с id из таблицы site_countries_statistics
            $scsIds = [];
            
            $result = SiteCsId2office::find()
                ->select('cs_id')
                ->where(
                    ['office_id' => $officeId]
                )
                ->asArray()
                ->all();
            
            foreach ($result as $item){
                $scsIds[] = $item['cs_id'];
            }

            //создаем запрос на выборку запросов с id из массива
            $query = $modelClass::find()
                ->select(
                    [
                        'id',
                        'type'
                    ]
                )
                ->where(
                    ['id' => $scsIds]
                );    
            
        } else {
            //создаем запрос на выборку запросов с id из массива
            $query = $modelClass::find()
                ->select(
                    [
                        'id',
                        'type'
                    ]
                ); 
        }

        //формируем xml
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
        
    }

}
